package com.api.v1.training.user.util;

public enum UserType {
	
	CUSTOMER, ADMIN;

}
