package com.api.v1.training.user;


import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.repository.CustomerRepository;

@SpringBootApplication
public class Application {
	
	@Autowired
	private CustomerRepository customerRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
   
    @PostConstruct
    private void initDb() {
      
      User user = new User();
      user.setUniquecode("1234ABC");
      customerRepository.save(user);
    }
}
