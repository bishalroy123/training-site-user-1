package com.api.v1.training.user.request;

import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.Userprofile;

public class CustomerRequest {

	private User user;
	private Userprofile userprofile;
	

	public CustomerRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CustomerRequest(User user, Userprofile userprofile) {
		super();
		this.user = user;
		this.userprofile = userprofile;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Userprofile getUserprofile() {
		return userprofile;
	}
	public void setUserprofile(Userprofile userprofile) {
		this.userprofile = userprofile;
	}

}
