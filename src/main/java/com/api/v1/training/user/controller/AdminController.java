package com.api.v1.training.user.controller;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;

@SpringBootApplication
@RestController
@RequestMapping("trainingsite/admin")
public class AdminController {
   
	/*
    @RequestMapping("/")
	public ModelAndView firstPage() {
		return new ModelAndView("welcome");
	} */
    
    @RequestMapping(value = { "/userWiseCourseList" }, method = RequestMethod.GET)
	public String userWiseCourseList() {
    	// TODO return type from string to list
    	return "user wise course list";
	}
    
    
}